package chapter3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Ex6 {

    public static void luckySort(ArrayList<String> strings, Comparator<String> comp){

        ArrayList<String> list = new ArrayList<>(strings);
        list.sort(comp);
        while(!list.equals(strings)){
            Collections.shuffle(strings);
        }
    }
}
