package chapter3;

import java.util.Arrays;
import java.util.List;

public class Ex3 {

    interface IntSequence {

        default boolean hasNext() {
            return true;
        }

        int next();

        static IntSequence of(int... list) {
            return new IntSequence() {
                private int pos = 0;

                @Override
                public boolean hasNext() {
                    return pos < list.length;
                }

                @Override
                public int next() {
                    return hasNext() ? list[pos++] : 0;
                }
            };
        }

        static IntSequence constant(int con) {
            return ()->con;
        }
    }

}
