package chapter3;

import java.util.Arrays;

public class Ex1 {

    interface Measurable {
        double getMeasure();

    }

    static class Employee implements Measurable {
        private double salary;

        public Employee(double salary) {
            this.salary = salary;
        }

        @Override
        public double getMeasure() {
            return this.salary;
        }


        public static Measurable largest(Measurable[] objects) {
            return Arrays.asList(objects).stream()
                    .max((o1, o2) -> (int) (o1.getMeasure() - o2.getMeasure())).get();
        }

        static double average(Measurable[] objects) {
            return Arrays.asList(objects).stream().mapToDouble(o -> o.getMeasure()).average().getAsDouble();
        }
    }
}
