package chapter3;

public class Ex8 implements Runnable{

    public Ex8(String target, int n) {
        this.target = target;
        this.n = n;
    }

    private String target;
    private int n;
    @Override
    public void run() {
        for(int i = 0;i<this.n;i++){
            System.out.println("witaj "+this.target);
        }
    }

    public static void main(String[] args) {
        new Thread(new Ex8("Krzys",2)).start();
        new Thread(new Ex8("Anetka",10)).start();
    }
}
