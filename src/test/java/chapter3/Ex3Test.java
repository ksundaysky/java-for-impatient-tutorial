package chapter3;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class Ex3Test {

    @Test
    public void testIntStream() {
        var data = new int[]{9, 8, 7, 5, 5, 1, 3, 5};
        var seq = Ex3.IntSequence.of(data);
        for (var val : data) {
            assertTrue(seq.hasNext());
            assertEquals(val, seq.next());
        }
        assertFalse(seq.hasNext());
    }

    @Test
    public void testExceedIntStream() {
        var seq = Ex3.IntSequence.of();
        assertFalse(seq.hasNext());
        assertEquals(0, seq.next());
        assertEquals(0, seq.next()); // verifying that it happens multiple times
    }

    @Test
    public void testConstantIntStream() {
        final var value = 1;
        var seq = Ex3.IntSequence.constant(value);
        for (var i = 0; i < 20; i++) {
            assertTrue(seq.hasNext());
            assertEquals(value, seq.next());
        }
    }

}