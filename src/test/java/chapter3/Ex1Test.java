package chapter3;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.Arrays;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.*;

class Ex1Test {

    @ParameterizedTest
    @CsvSource({"'0.0,20.0', 10.0,",
            "'10.0,20.0,30.0', 20.0",
            "'1.0,1.0,1.0,3.0,3.0,3.0', 2.0"})
    void testEmployeeAverageSalaryEquals(String data, double avarage){
        Ex1.Measurable[] arr = Arrays.stream(data.split(","))
                .map(e -> new Ex1.Employee(Double.parseDouble(e)))
                .toArray(Ex1.Measurable[]::new);
        assertEquals(Ex1.Employee.average(arr),avarage);
    }

    @ParameterizedTest
    @CsvSource({"'0.0,20.0,5.0', 10.0,",
            "'10.0,20.0,30.0,1.0', 20.0",
            "'1.0,1.0,1.0,3.0,3.0,3.0,3.0', 2.0"})
    void testEmployeeAverageSalaryNotEquals(String data, double avarage){
        Ex1.Measurable[] arr = Arrays.stream(data.split(","))
                .map(e -> new Ex1.Employee(Double.parseDouble(e)))
                .toArray(Ex1.Measurable[]::new);
        assertNotEquals(Ex1.Employee.average(arr),avarage);
    }

    @ParameterizedTest
    @CsvSource({"'0.0,20.0,5.0', 20.0",
            "'10.0,20.0,30.0,1.0', 30.0",
            "'1.0,1.0,1.0,3.0,3.0,3.0,3.0', 3.0"})
    void testEmployeeLargestSalaryEquals(String data, double max){
        Ex1.Measurable[] arr = Arrays.stream(data.split(","))
                .map(e -> new Ex1.Employee(Double.parseDouble(e)))
                .toArray(Ex1.Measurable[]::new);
        assertEquals(Ex1.Employee.largest(arr).getMeasure(), max);
    }


    @ParameterizedTest
    @CsvSource({"'0.0,21.0,5.0', 20.0",
            "'10.0,20.0,31.0,1.0', 30.0",
            "'1.0,1.0,1.0,4.0,3.0,3.0,3.0', 3.0"})
    void testEmployeeLargestSalaryNotEquals(String data, double max){
        Ex1.Measurable[] arr = Arrays.stream(data.split(","))
                .map(e -> new Ex1.Employee(Double.parseDouble(e)))
                .toArray(Ex1.Measurable[]::new);
        assertNotEquals(Ex1.Employee.largest(arr).getMeasure(), max);
    }


}