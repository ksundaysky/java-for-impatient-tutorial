package chapter3;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class Ex6Test {

    @Test
    public void testLuckySort(){
        ArrayList<String> strings = new ArrayList<>();
        strings.add("first");
        strings.add("second");
        strings.add("third");
        strings.add("fourther");
        strings.add("one");
        strings.add("dd");
        strings.add("ddddd");

        Ex6.luckySort(strings,(o1,o2)->o1.length()-o2.length());

        assertTrue("fourther".equals(strings.get(strings.size()-1)));
        assertTrue("dd".equals(strings.get(0)));
    }
}